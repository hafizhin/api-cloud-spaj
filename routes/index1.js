// var il3 = require('../contoh');


// router.get('http://localhost:5000/getuser', il3.getUser);

// router.post('http://localhost:5000/loaddpp', il3.loaddpp);


'use strict';

module.exports = function(app) {
    var todoList = require('../contoh');
    var transfer = require('../insert')

    app.route('/users')
        .get(todoList.getUser);

    app.route('/data')
        .post(todoList.loaddpp);

    app.route('/transfer')
        .post(transfer.insertTransaksi);
};