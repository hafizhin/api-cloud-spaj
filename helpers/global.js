'use strict';

const sql = require('mssql');

const dbibonline = {
    user:"sa",
	password:"P@ssw0rd",
	server:"104.250.105.214",
	database:"DBIBONLINE",
	connectionTimeout : 300000,
	requestTimeout :300000,
	port:1433
};

const dbworkflow = {
    user:"sa",
	password:"P@ssw0rd",
	server:"104.250.105.214",
	database:"DBWorkflow",
	requestTimeout: 30000,
	port:1433
};

exports.exec_qry = async (obj) => {
    let connsetting = null;
    let properti = obj.returnProperti;
    switch(obj.conn){
        case 'dbibonline':
            connsetting = dbibonline;
        break;
        case 'dbworkflow':
            connsetting = dbworkflow;
        break;
    }
    const pool = await sql.connect(connsetting);
    try {
        let result = await pool
            .request()
            .query(obj.qry);
        let hasil = `{"success": true, "${properti}": ${JSON.stringify((result === undefined)?[]:result)}}`;
        console.log(hasil);
        return JSON.parse(hasil);
    } catch (err) {
        return {success: false, errMsg: err};
    } finally {
        pool.close();
        sql.close();
    }
};

exports.exec_sp = async (obj) => {
    let connsetting = null;
    let properti = obj.returnProperti;
    switch(obj.conn){
        case 'dbibonline':
            connsetting = dbibonline;
        break;
        case 'dbworkflow':
            connsetting = dbworkflow;
        break;
    }
    const pool = await sql.connect(connsetting);
    try {
        
        const request = new sql.Request(pool)
        obj
            .params
            .forEach(obj_data => {
                request.input(obj_data.param, obj_data.type, obj_data.value)
            });
        let result = await request.execute(obj.sp);
        let hasil = `{"success": true, "${properti}": ${JSON.stringify(result[0])}}`;
        console.log(hasil);
        return JSON.parse(hasil);
    } catch (err) {
        return {success: false, errMsg: err};
    } finally {
        pool.close();
        sql.close();
    }
};

exports.create_conn_trans = async (obj) => {

    try {
        let connsetting = null;
        switch(obj.conn){
            case 'dbibonline':
                connsetting = dbibonline;
            break;
            case 'dbworkflow':
                connsetting = dbworkflow;
            break;
        }
        const pool = await sql.connect(connsetting);
        return pool;
    } catch (e) {
        console.log('hirawan x');
        console.log(e.message);
    }
    
};

exports.exec_qry_trans = async (obj, trans_var) => {
    try {
        let result = await new sql.Request(trans_var).query(obj.qry);
        return result;
    } catch (err) {
        console.log('hirawan y');
        return err;
    }
};

