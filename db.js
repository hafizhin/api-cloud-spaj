var sqlDB = require("mssql");

module.exports.dbConfig = {
    user: 'sa',
    password: 'P@ssw0rd',
    server: '104.250.105.214', 
    database: 'dbibonline_',
	connectionTimeout : 300000,
	requestTimeout :300000,
	port:1433
};

module.exports.executeSql = function(sql,callback) {
	var conn = new sqlDB.Connection(dbConfig);
	conn.connect().then(function() {
		// Query
		var request = new sqlDB.Request(conn);
		request.query(sql).then(function(recordset) {
			conn.close();
			callback(recordset);
		}).catch(function(err) {
			// ... error checks
			console.log(err);
			conn.close();
			callback(null,err); 
		});
	 
	}).catch(function(err) {
		console.log(err);
		conn.close();
		callback(null,err); 
	});
};