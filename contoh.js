
var sql = require("mssql");

exports.getUser = function (req,res) {
        // create Request object
        var request = new sql.Request();
           
        // query to the database and get the records
        request.query('SELECT TOP 3 * FROM dbo.nb_spaj_kuesion_medis;', function (err, recordset) {
            
            if (err) console.log(err)

            // send records as a response
            res.send(recordset);
            
        });
        // console.log('sukses');
}

exports.loaddpp = async (req, res) => {
    try {
        let kode_produk = req.body.kode_produk;
        let param1 = req.body.param1;
        let no_registrasi = req.body.no_registrasi;

        let result = {
            success: false,
            carapembayaran: [],
            periodebayar: [],
            jeniskelamin: [],
            status: [],
            hub: [],
            manfaat: [],
            showpp: [],
            masabayar: []
        };

        let objsp = {
            conn: 'dbibonline',
            sp: 'dbo.sp_get_reff_acc',
            returnProperti: 'rows',
            params: [
                {
                    param: 'reff',
                    type: sql.VarChar(100),
                    value: 'carapembayaran'
                },
                {
                    param: 'kode_produk',
                    type: sql.VarChar(100),
                    value: kode_produk
                },
                {
                    param: 'param1',
                    type: sql.VarChar(100),
                    value: param1
                }
            ]
        }

        let hasil1 = await helper.exec_sp(objsp);
        result.carapembayaran = hasil1.rows;
        var param11 = '';
        if (kode_produk === '02008' || kode_produk === '04008') {
            param11 = 'DASETERA';
        } else {
            param11 = '';
        }
        var reff = '';
        if (kode_produk === 'UL000005' || kode_produk === 'UL000006' || kode_produk === 'UL000008' || kode_produk === 'UL000010' || kode_produk === 'UL000012' || kode_produk === 'UL000014') {
            reff = 'periodebayarSin';
        } else {
            reff = 'periodebayar';
        }

        objsp.params[0].value = reff;
        objsp.params[2].value = param11;

        let hasil2 = await helper.exec_sp(objsp);
        result.periodebayar = hasil2.rows;

        objsp.params[0].value = 'jeniskelamin';
        objsp.params[2].value = param1;
        
        let hasil3 = await helper.exec_sp(objsp);
        result.jeniskelamin = hasil3.rows;

        objsp.params[0].value='status';
        let hasil4 = await helper.exec_sp(objsp);
        result.status = hasil4.rows;

        objsp.params[0].value='manfaat';
        let hasil5 = await helper.exec_sp(objsp);
        result.manfaat = hasil5.rows;

        objsp.params[0].value='hub';
        let hasil6 = await helper.exec_sp(objsp);
        result.hub = hasil6.rows;

        objsp.qry = `select a.* from nb_nasabah_byrpremi a left join nb_spaj b on a.id_aplikasi=b.id_aplikasi
        where b.TransNo='${no_registrasi}'`;
        let hasil7 = await helper.exec_qry(objsp);
        result.showpp = hasil7.rows[0];

        objsp.params[0].value='masabayar';
        objsp.params[2].value='';
        let hasil8 = await helper.exec_sp(objsp);
        result.masabayar = hasil8.rows;
        result.success = true;

        res.json(result);

    } catch (err) {
        res.send(err);
    }
};